require.config({
    urlArgs: "bust=" + (new Date()).getTime(),
    baseUrl: './js'
});


require(['pixi','jquery'], function(PIXI) {
    "use strict";


    var viewModes = {
            index:1000,
            detail:2000
        },
        rotationSpeed = 0,
        dim = [1024, 768],
        INDEX_ZOOM = 0.25,
        SINGLE_ZOOM = 1,
        viewMode = viewModes.index,
        easing = 10,
        rotors = [],
        scene = null,
        stage = new PIXI.Stage(0x000000),
        renderer = null,
        files = [
            "assets/images/S1.png",
            "assets/images/S2.png",
            "assets/images/S3.png",
            "assets/images/S4.png",
            "assets/images/S5.png",
            "assets/images/S6.png",
            "assets/images/S7.png",
            "assets/images/S8.png",
            "assets/images/S9.png",
            "assets/images/S10.png",
            "assets/images/S4.png",
            "assets/images/S12-2.png"
        ],
        targetScale = INDEX_ZOOM,
        targetPos = {},
        indexPos = {};

    function animate() {
        requestAnimFrame( animate );

        for (var i =0; i < rotors.length;i++) {
            var roto = rotors[i];
            roto.rotation += rotationSpeed;
        }


        scene.x += (targetPos.x - scene.x)/ easing ;
        scene.y += (targetPos.y - scene.y)/ easing ;

        scene.scale.y += (targetScale - scene.scale.y)/easing ;
        scene.scale.x = scene.scale.y;

        renderer.render(stage);
    }


    var startAngle = null,
        rotating = null,
        startRotation = null,
        prevAngle = null,
        deltaDeg = null,
        prevMoment = null,
        deltaMoment = null;



    function onStartClickRoto(e, sprite) {
        var pos = convertPos(e, sprite),
            angle = deg(pos);

        rotating = sprite;
        startAngle = angle;
        startRotation = sprite.rotation;
        rotationSpeed = 0;
        deltaDeg = null;
        deltaMoment = null;
    }

    function onMoveRoto(e, sprite) {
        if (rotating == sprite) {
            var pos = convertPos(e, sprite),
                angle = deg(pos),
                dAngle = angle - startAngle;

            sprite.rotation = startRotation + dAngle / 180 * Math.PI;


            deltaDeg = angle - prevAngle;
            deltaMoment = (+new Date()) - prevMoment;

            prevAngle = angle;
            prevMoment = +new Date();
        }
    }

    function onRotoUp(e, sprite) {
        rotating = null;
        startAngle = null;

        if ((+new Date()) - prevMoment > 100) {
            deltaDeg = 0;
        }

        if (!deltaDeg || !deltaMoment || isNaN(deltaDeg) || isNaN(deltaMoment) || deltaMoment == 0) {
            console.log('set 0');
            rotationSpeed = 0;
        } else {
            rotationSpeed = (deltaDeg/deltaMoment)/1.5;
        }
    }

    function deg(pos) {
        var deg = Math.atan(pos.y / pos.x) / Math.PI * 180,
            result = deg;

        if ((pos.y > 0 && pos.x < 0) || (pos.y < 0 && pos.x < 0)){
            result += 180;
        } else if (pos.y < 0 && pos.x > 0) {
            result += 360;
        }

        return result;
    }

    function convertPos(e, sprite) {
        var p = e.getLocalPosition(scene),
            c = {
                x : (p.x - sprite.x) / sprite.width  * 2,
                y : (p.y - sprite.y) / sprite.height * 2
            };

        return c;
    }

    function createRotorelief(options) {
        var texture = PIXI.Texture.fromImage(options.src),
            rotoSprite = new PIXI.Sprite(texture);

        rotoSprite.interactive = true;
        rotoSprite.anchor.y = rotoSprite.anchor.x = 0.5;

        rotoSprite.height = rotoSprite.width = Math.min(renderer.height, renderer.width);

        rotoSprite.x = options.x;
        rotoSprite.y = options.y;

        rotoSprite.mousedown = rotoSprite.touchstart = function (e) {
            var c = convertPos(e, rotoSprite);

            if (viewMode == viewModes.index) {
                $(document).trigger('ROTO_REQUESTED', {
                    roto: e.target,
                    pos: c
                });
            } else {
                onStartClickRoto(e, rotoSprite);
            }
        };

        rotoSprite.mousemove = function (e) {
            if (viewMode == viewModes.detail) {
                onMoveRoto(e, rotoSprite);
            }
        };

        stage.mouseup = function (e) {
            if (viewMode == viewModes.detail) {
                onRotoUp(e, rotoSprite);
            }
        };

        return rotoSprite;
    }

    function showRoto(rotoSprite) {
        viewMode = viewModes.detail;
        targetScale = SINGLE_ZOOM;
        targetPos.x = -rotoSprite.x + rotoSprite.width/2  + (renderer.width  - rotoSprite.width) /2;
        targetPos.y = -rotoSprite.y + rotoSprite.height/2 + (renderer.height - rotoSprite.height)/2;
    }

    function showIndex() {
        console.log('showIndex');
        viewMode = viewModes.index;
        targetScale = INDEX_ZOOM;
        targetPos.x  = indexPos.x;
        targetPos.y  = indexPos.y;
    }

    function init() {

        renderer = PIXI.autoDetectRenderer(
            dim[0],
            dim[1],
            null,
            false,
            true
        );

        scene = new PIXI.DisplayObjectContainer();

        document.body.appendChild(renderer.view);

        requestAnimFrame( animate );

        var x = 0, y = 0, margin = 128;

        for (var i = 0; i < files.length;i++) {
            var radius = Math.min(renderer.height, renderer.width);
            rotors.push(createRotorelief({
                src: files[i],
                x: x * (radius  + margin),
                y: y * (radius  + margin)
            }));
            x++;
            if (x >= 4) {
                x = 0;
                y++;
            }
        }

        stage.interactive = true;
        stage.addChild(scene);

        scene.scale.x = scene.scale.y = INDEX_ZOOM;
        for (var i=0; i < rotors.length;i++) {
            var rotor = rotors[i];
            scene.addChild(rotor);
        }

        indexPos = {
            x: (renderer.width - scene.width)   / 2 + rotors[0].width/2 * scene.scale.x,
            y: (renderer.height - scene.height) / 2 + rotors[0].width/2 * scene.scale.y
        };

        targetPos.x = scene.x = indexPos.x;
        targetPos.y = scene.y = indexPos.y;

        showRoto(rotors[7]);

        $(document).on('ROTO_REQUESTED', function (e, data) {
           showRoto(data.roto);
        });

        $(document).on('INDEX_REQUESTED', function (e, data) {
            showIndex();
        });
    }

    $(function () {
        init();
    });
});